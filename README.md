# Python pipenv base container

This is a base container containing python with pipenv pre-installed,

By default, this container will run as the non-root pipenv user.
